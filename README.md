## В данном репозитории находятся файлы для сборки и последующего деплоя тестового приложения (веб-странички) в кластер.

Манифест для деплоя в Kubernetes [здесь](https://gitlab.com/pitonixxx/diploma_nd9_app/-/blob/main/kubernetes/nginx-app.yml)

Адрес [веб-приложения](http://84.201.130.110/)

Образ [Докер](https://gitlab.com/pitonixxx/diploma_nd9_app/-/blob/main/Dockerfile)

Build [пайплайн](https://gitlab.com/pitonixxx/diploma_nd9_app/-/jobs/2914230092)

Deploy [пайплайн](https://gitlab.com/pitonixxx/diploma_nd9_app/-/jobs/2914230095)

Приложение собирается при коммите с тегом в Gitlab CI, образ Docker загружен в Gitlab Registry, Gitlab посредству агента связан с кластером YC, сборка автоматизирована. Файлы приложения загружаются в папку "html", откуда забираются на сервер.